---
id: chapter01
title: Chapter 1: Prerequisites
sidebar_label: Chapter 1: Prerequisites
---

The Generic App gem has the same development environment prerequisites as the Rails Neutrino tool.  Follow Chapter 1 of the [Rails Neutrino Tutorial](https://www.railsneutrino.com/docs/chapter01), and then continue on to the next chapter in this tutorial.
